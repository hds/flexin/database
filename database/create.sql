CREATE GRAPH flexin;
ALTER DATABASE flexin SET graph_path TO flexin;

-- Node labels --

CREATE VLABEL organisation;

CREATE VLABEL person;
CREATE VLABEL manager INHERITS (person);

CREATE VLABEL property; -- System wide properties (ex : int, string...)
CREATE VLABEL custom_property INHERITS (property); -- Organisation defined properties

CREATE VLABEL category;

CREATE VLABEL item;

-- Node constraints --

  -- organisation --
CREATE CONSTRAINT "Organisation has name" ON organisation ASSERT name IS NOT NULL;
CREATE CONSTRAINT "Organisation name is unique" ON organisation ASSERT name IS UNIQUE;

  -- person --
CREATE CONSTRAINT "Person has lastname" ON person ASSERT lastname IS NOT NULL;
CREATE CONSTRAINT "Person has firstname" ON person ASSERT firstname IS NOT NULL;
CREATE CONSTRAINT "Person has email" ON person ASSERT email IS NOT NULL;

  -- manager --
CREATE CONSTRAINT "User has login" ON manager ASSERT login IS NOT NULL;
CREATE CONSTRAINT "User login is unique" ON manager ASSERT login IS UNIQUE;
CREATE CONSTRAINT "User has password" ON manager ASSERT password IS NOT NULL;

  -- property --
CREATE CONSTRAINT "Property has name" ON property ASSERT name IS NOT NULL;
CREATE CONSTRAINT "Property name is unique" ON property ASSERT name IS UNIQUE;
-- CREATE CONSTRAINT "Custom property must not have same name as a system-wide property" ON custom_property ASSERT (OPTIONAL MATCH (n:property {name: name}) RETURN n) IS NULL;

  -- category --
CREATE CONSTRAINT "Category has name" ON category ASSERT name IS NOT NULL;
-- Category unique for an organisation ?

-- Edge labels --

CREATE ELABEL manage; -- Manager and organisations can manage other organisations

CREATE ELABEL subcategory_of; -- Category hierarchy

CREATE ELABEL attribute; -- Edge to link categories and properties : required attributes

CREATE ELABEL is_in; -- Item hierarchy
CREATE ELABEL loan_to; -- Item -> Person (loan)

-- Edge constraints --

CREATE CONSTRAINT "A required attribute must have a name" ON attribute ASSERT name IS NOT NULL;

CREATE CONSTRAINT "Must specify a date when placing item in other" ON is_in ASSERT register_date IS NOT NULL;
CREATE CONSTRAINT "Must specify a start date for a loan" ON loan_to ASSERT start_date IS NOT NULL;
CREATE CONSTRAINT "Must specify an end date for a loan" ON loan_to ASSERT end_date IS NOT NULL;
