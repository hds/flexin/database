FROM bitnine/agensbrowser
MAINTAINER remy.huet@etu.utc.fr

USER agens

COPY database/ /database/

RUN ag_ctl start; sleep 5; \
    agens -c "CREATE DATABASE flexin;"; \
    agens -c "CREATE USER flexin WITH PASSWORD 'flexin'; \
	ALTER DATABASE flexin OWNER TO flexin;"; \
    agens -U flexin -d flexin -f /database/create.sql; \
    ag_ctl stop

EXPOSE 5432

ENTRYPOINT ["/home/agens/scripts/entrypoint.sh"]
