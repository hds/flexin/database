# FLEXible INventory (FLEXIN) database system

This repository contains the definition files of the FLEXIN database.

## How does it work ?

FLEXIN is using [AgensGraph](https://github.com/bitnine-oss/agensgraph) database management system.

To create the database, please make sure to install AgensGraph

## Why using a graph database system ?

FLEXible INventory system allows complex hierarchy between organisations and objects.
Thus, we need to use a database management system that allows simple and powerful research among graphs.

We choose AgensGraph for its performance and ability to use both graphs and postgres relational SQL queries.

## Setting up the database

//TODO

